const Header = ({className}) => {
    return (
        <header className={className}>
            <h2>Lista de Tarefas</h2>
        </header>
    )
}

export default Header;