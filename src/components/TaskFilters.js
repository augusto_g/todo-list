import { useState, useEffect } from 'react';
import StyledInputTxt from "./InputTxt"
import styled from "styled-components";
import StyledInputRadio from "./InputRadio";

const TaskFilters = ({className, filterTasks}) => {
    const [nameFilter, setNameFilter] = useState('');
    const [radioFilter, setRadioFilter] = useState('all');

    const updateInput = (e) => {
        setNameFilter(e.target.value);
    }

    const updateRadio = (e) => {
        setRadioFilter(e.target.value);
    }

    useEffect(() => {
		filterTasks(nameFilter, radioFilter);
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [nameFilter, radioFilter]);

    return (
        <div className={className}>
            <FilterTxt 
                type="text"
                id="filterTaskName"
                placeholder="Filtrar"
                onChange={updateInput}
            />
            <FilterRadioGroup>
                <StyledInputRadio
                    id={"allRb"}
                    name={"viewTask"}
                    value={"all"}
                    label={"Todas"}
                    checked={radioFilter === "all"}
                    onChange={updateRadio}
                />
                <StyledInputRadio
                    id={"todoRb"}
                    name={"viewTask"}
                    value={"todo"}
                    label={"Para fazer"}
                    checked={radioFilter === "todo"}
                    onChange={updateRadio}
                />
                <StyledInputRadio
                    id={"doneRb"}
                    name={"viewTask"}
                    value={"done"}
                    label={"Feitas"}
                    checked={radioFilter === "done"}
                    onChange={updateRadio}
                />
            </FilterRadioGroup>
        </div>
    );
}

const FilterRadioGroup = styled.div`
    display: flex;
    width: 40%;
    margin: 1rem 1.5rem 0 0;
    justify-content: space-between;
`;

const FilterTxt = styled(StyledInputTxt)`
    width: 70%;
    font-size: 1rem;
    margin: 1rem 1rem 0 2rem;
    border-color: black;
`;

const StyledTaskFilters = styled(TaskFilters)`
    border: 0 solid lightgrey;
    border-top-width: 1px;
    display: flex;
    justify-content: space-between;
`;

export default StyledTaskFilters;