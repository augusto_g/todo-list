import styled from "styled-components";
import PropTypes from 'prop-types';

const InputRadio = ({className, id, name, label, value, checked, onChange}) => {
    return(
        <div className={className}>
            <input
                type="radio"
                id={id}
                name={name}
                value={value}
                checked={checked}
                onChange={onChange}
            />
            <label htmlFor={id}>{label}</label>
        </div>
    );
}

InputRadio.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.string,
    checked: PropTypes.bool
}

InputRadio.defaultProps = {
    id: 'id',
    name: 'group',
    label: 'option',
    value: '',
    checked: false
}

const StyledInputRadio = styled(InputRadio)`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export default StyledInputRadio;