import { useState } from 'react';
import styled from "styled-components";
import StyledInputTxt from "./InputTxt"

const TaskInput = ({className, saveTask}) => {
    const [textName, setTextName] = useState('');

    const addTask = () => {
        let name = textName.trim();
        if (name.length) {
            saveTask(name);
            setTextName('');
        }
    }

    const updateInput = (e) => {
        setTextName(e.target.value);
    }

    return (
        <div className={className}>
            <AddInput
                type="text"
                id="newTask"
                placeholder="Nova tarefa"
                value={textName}
                onChange={updateInput}
            />
            <AddButton
                type="button"
                value="Adicionar"
                onClick={addTask}
            />
        </div>
    )
}

const AddButton = styled.input`
    min-width: 15vmin;
    border-width: 0;
    margin: 0.5rem 0;
    border-radius: 3px;
    font-size: 1.2rem;
    background-color: #478dfb;
    color: white;
    cursor: pointer;

    &:hover {
        background-color: #2a70de;
    }
`;

const AddInput = styled(StyledInputTxt)`
    width: 85%;
    font-size: 1.2rem;
    margin: 1rem;
    margin-right: 1.5rem;
    border-color: black;
`;

const StyledTaskInput = styled(TaskInput)`
    display: flex;
    margin: 1rem;
    justify-content: space-between;
`;

export default StyledTaskInput;