import { useState, useEffect } from 'react';
import { v4 as id} from 'uuid';
import StyledTaskList from "./components/TaskList";
import Header from "./components/Header";
import './App.css';

function App() {
	const loadStorage = () => {
		let tasks = localStorage.getItem('tasks');
		if (tasks !== null) {
			tasks = JSON.parse(tasks);
		}
		else {
			tasks = [];
		}
		return tasks;
	}

	const [tasks, setTasks] = useState(loadStorage());
	const [filteredTasks, setFilteredTasks] = useState(tasks)
	const [filters, setFilters] = useState({nameFilter: '', radioFilter: 'all'});

	const saveTask = (taskName) => {
		setTasks(
			tasks =>
			[{
				id: id(),
				done: false,
				name: taskName
			},
			...tasks]
		);
	}

	const updateTask = (updatedTask) => {
		let updatedTasks = tasks.map((task) => {
			if (task.id === updatedTask.id) {
				task.name = updatedTask.name;
				task.done = updatedTask.done;
			}
			return task;
		})
		setTasks(updatedTasks);
	}

	const removeTask = (id) => {
		let updatedTasks = tasks.filter((task) => task.id !== id);
		setTasks(updatedTasks);
	}

	const filterTasks = (nameFilter, radioFilter) => {
		nameFilter = nameFilter.trim();
		setFilters({nameFilter, radioFilter});
		let filteredList = tasks;
		if (nameFilter.length){
			filteredList = tasks.filter((task) => task.name.includes(nameFilter));
		}
		
		if (radioFilter === "todo") {
			filteredList = filteredList.filter((task) => !task.done);
		}
		else if (radioFilter === "done") {
			filteredList = filteredList.filter((task) => task.done);
		}
		setFilteredTasks(filteredList);
	}

	useEffect(() => {
		filterTasks(filters.nameFilter, filters.radioFilter);
		localStorage.setItem("tasks", JSON.stringify(tasks));
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [tasks]);

	return (
		<div className="App">
			<Header />
			<StyledTaskList 
				tasks={filteredTasks}
				saveTask={saveTask} 
				removeTask={removeTask}
				updateTask={updateTask}
				filterTasks={filterTasks}
			/>
		</div>
	);
}

export default App;
