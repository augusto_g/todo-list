import styled from "styled-components";
import PropTypes from 'prop-types';
import StyledTaskItem from './TaskItem';
import StyledTaskInput from "./TaskInput";
import StyledTaskFilters from "./TaskFilters"

const TaskList = ({className, tasks, saveTask, removeTask, updateTask, filterTasks}) => {
    return (
        <div className={className}>
            <StyledTaskInput saveTask={saveTask}/>
            <StyledTaskFilters filterTasks={filterTasks}/>
            <List>
                {tasks.map((task) => {
                    return (
                        <li key={task.id}>
                            <StyledTaskItem task={task} removeTask={removeTask} updateTask={updateTask} />
                        </li>
                    )
                })
            }
            </List>
        </div>
    )
}

TaskList.propTypes = {
    tasks: PropTypes.array
}

TaskList.defaultProps = {
    tasks: []
}

const List = styled.ul`
    list-style-type: none;
    padding: 0;
    border: 0 solid lightgrey;
    border-top-width: 1px;
`;

const StyledTaskList = styled(TaskList)`
    background-color: white;
    width: 50vmax;
    border-radius: 3px;
    box-shadow: 0 0 20px #cacaca;
    margin-bottom: 5%;
    min-height: 80vh;
`;

export default StyledTaskList;