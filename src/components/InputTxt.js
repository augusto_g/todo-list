import styled from "styled-components";

const StyledInputTxt = styled.input`
    border-width: 0 0 1px 0;
    outline: none;
`;

export default StyledInputTxt;