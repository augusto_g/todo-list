import styled from "styled-components";
import { useState } from 'react';
import PropTypes from 'prop-types';
import StyledInputTxt from "./InputTxt"

const TaskItem = ({className, task, removeTask, updateTask}) => {
    const [taskName, setTaskName] = useState(task.name);
    const [editedTaskName, setEditedTaskName] = useState(taskName);

    const toggleTask = (isDone) => {
        task.done = isDone;
        updateTask(task);
    }

    const updateInput = (e) => {
        setEditedTaskName(e.target.value);
    }

    const updateTaskName = () => {
        let name = editedTaskName.trim();
        if (name.length) {
            setEditedTaskName(name);
            setTaskName(name);
            task.name = name;
            updateTask(task);
        }
        else {
            setEditedTaskName(taskName);
        }
    }

    return (
        <TaskDiv className={className} done={task.done}>
            <NameInput 
                type="text"
                value={editedTaskName}
                onChange={updateInput}
                onBlur={updateTaskName}
            />
            {task.done?
            <ReturnBt onClick={() => toggleTask(false)}>⮌</ReturnBt>:
            <DoneBt onClick={() => toggleTask(true)}>✓</DoneBt>
            }
            <RemoveBt onClick={() => removeTask(task.id)}>✕</RemoveBt>
        </TaskDiv>
    );
}

TaskItem.propTypes = {
    task: PropTypes.object
}

TaskItem.defaultProps = {
    task: {
        name: ''
    }
}

const TaskOpts = styled.button`
    border-width: 0;
    background-color: transparent;
    cursor: pointer;
    font-size: 1.2rem;
    min-width: 8vmin;
    border-left-width: 1px;

    &:hover {
        color: white;
    }
`;

const ReturnBt = styled(TaskOpts)`
    color: blue;

    &:hover {
        background-color: lightblue;
    }
`;

const DoneBt = styled(TaskOpts)`
    color: green;

    &:hover {
        background-color: lightgreen;
    }
`;

const RemoveBt = styled(TaskOpts)`
    color: red;

    &:hover {
        background-color: lightcoral;
    }
`;

const NameInput = styled(StyledInputTxt)`
    font-size: 1.2rem;
    margin: 1rem 1.5rem;
    width: 100%;
    background-color: transparent;
    color: inherit;
    border-color: transparent;

    &:focus {
        border-color: black;
    }
`;

const TaskDiv = styled.div`
    ${({done}) => (
        done ? 
        "color: gray; background-color: #efefef;":
        "color: black; background-color: white;"
    )}
`;

const StyledTaskItem = styled(TaskItem)`
    border: 1px solid lightgray;
    border-radius: 3px;
    margin: 1rem;
    display: flex;
    justify-content: space-between;

    &:hover {
        box-shadow: 3px 3px 8px #cacaca;
    }
`;

export default StyledTaskItem;